#!/usr/bin/env python3

from bs4 import BeautifulSoup
import csv
import time
import urllib3


# url of main staff page
quote_page = open('url_target.txt').read().strip()
http = urllib3.PoolManager()
response = http.request('GET', quote_page)

soup = BeautifulSoup(response.data)

# get all the staff container divs and then the url links to their pages
targets = soup.find("div", {'id': 'new_content_container_715263'})
staff_links = targets.find_all({'a':'href'})

url_dict = {}
for sl in staff_links:
    name = sl.text
    try:
        lastName = name.split(',')[0].strip().upper()
        firstName = name.split(',')[1].strip().upper()
    except IndexError:
        lastName = name.split(' ')[0].strip().upper()
        firstName = name.split(' ')[1].strip().upper()
    nameKey = firstName + '_' + lastName
    urllink = sl['href']
    #url_dict['JOHN_DOE']
    url_dict[nameKey] = urllink

# collect all names in the correct format to access the above dict
target_names = []
with open('all_targets.txt') as f:
    for line in f:
        bits = line.strip().split(' ')
        names = [n.upper() for n in bits]
        nameK = "_".join(names)
        target_names.append(nameK)

# now iterate over this list and scrapte their staff page
all_data = []
# make a header row
all_data.append(['name', 'unit', 'location', 'branch', 'cat'])

for name in target_names:
    try:
        t_url = url_dict[name]
        # sleep to avoid getting in trouble....
        time.sleep(5)
        response = http.request('GET', t_url)
        soup = BeautifulSoup(response.data)
        data = [name, '', '', '', '']
        for tag in soup.find_all('meta'):
            # overwrite by index if find data
            try:
                if tag['name'] == "Research Unit":
                    researchU = tag['content'] 
                    data[1] = researchU
                if tag['name'] == 'ResearchLocation':
                    researchLoc = tag['content']
                    data[2] = researchLoc
                if tag['name'] == 'Research Branch':
                    researchB = tag['content']
                    data[3] = researchB
                if tag['name'] == 'scientistandresearcher.category':
                    researchCat = tag['content']
                    data[4] = researchCat
            except KeyError:
                pass
    except KeyError:
        # these names have no URL in hte url_dict
        data = [name, '', '', '', '']
    all_data.append(data)

with open('final_table.csv', 'w') as f:
    csv_writer = csv.writer(f)
    for line in all_data:
        csv_writer.writerow(line)
